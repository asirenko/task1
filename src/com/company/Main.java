package com.company;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //read file path from console input
        System.out.println("Введите адрес файла:");
        String filePath = "";
        Scanner scan = new Scanner(System.in);
        try {
            filePath = scan.nextLine();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //read numbers from file
        List<Integer> numbers = new ArrayList<>();
        try {
            numbers = readFile(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //sort numbers from min to max
        displayAnswer(sortMintoMax(numbers));
        //read numbers from file
        numbers = new ArrayList<>();
        try {
            numbers = readFile(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //sort numbers from max to min
        displayAnswer(sortMaxtoMin(numbers));
    }

    static List<Integer> readFile(String path) throws IOException, NumberFormatException {
        byte[] textBytes = Files.readAllBytes(Paths.get(path));
        String text = new String(textBytes, Charset.defaultCharset());
        String[] textNumbers = text.split(",");
        List<Integer> numbers = new ArrayList<Integer>();
        for (String number : textNumbers) {
            numbers.add(Integer.parseInt(number));
        }
        return numbers;
    }

    static List<Integer> sortMintoMax(List<Integer> numbers) {
        for (int i = 0; i < numbers.size(); i++) {
            for (int j = 0; j < numbers.size(); j++) {
                if (numbers.get(i) < numbers.get(j)) {
                    int swap = numbers.get(j);
                    numbers.set(j, numbers.get(i));
                    numbers.set(i, swap);
                }
            }
        }
        return numbers;
    }

    static List<Integer> sortMaxtoMin(List<Integer> numbers) {
        for (int i = 0; i < numbers.size(); i++) {
            for (int j = 0; j < numbers.size(); j++) {
                if (numbers.get(i) > numbers.get(j)) {
                    int swap = numbers.get(j);
                    numbers.set(j, numbers.get(i));
                    numbers.set(i, swap);
                }
            }
        }
        return numbers;
    }

    static void displayAnswer(List<Integer> numbers) {
        System.out.println("Ответ: ");
        for (Integer num : numbers) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
}
